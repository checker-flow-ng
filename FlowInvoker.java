import com.sun.source.util.AbstractTypeProcessor;
import com.sun.source.util.TreePath;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;

public class FlowInvoker extends AbstractTypeProcessor {
    //   AbstractTypeProcessor delegation
    @Override
    public final void typeProcess(TypeElement element, TreePath tree) {
        outputGraphviz(new GenerateControlFlow().scan(tree, null));
    }

    public final void outputGraphviz(ControlFlowNode node) {
        // TODO: Recursion is bad, yo

        if (node == null) {
            System.out.println("// null node");
            return;
        }

        String thisAddr = "n" + Integer.toString(System.identityHashCode(node));

        ControlFlowNode successor = node.getSuccessor();
        if (successor != null) {
            System.out.println(String.format("%s->n%d",
                        thisAddr,
                        System.identityHashCode(successor)));
            outputGraphviz(successor);
        }

        ControlFlowNode elseSuccessor = node.getElseSuccessor();
        if (elseSuccessor != null) {
            System.out.println(String.format("%s->%d",
                        thisAddr,
                        System.identityHashCode(elseSuccessor)));
            outputGraphviz(elseSuccessor);
        }

        System.out.println(node.getExceptionSuccessors().size() + " exception successors");
        for (Map.Entry<Throwable, ControlFlowNode> entry :
                node.getExceptionSuccessors().entrySet()) {
            ControlFlowNode exNode = entry.getValue();
            if (exNode != null) {
                System.out.println(String.format("%s->%d",
                            thisAddr,
                            System.identityHashCode(exNode)));
                outputGraphviz(exNode);
            }
        }

        System.out.println(node.getPredecessors().size() + " predecessors");
        for (ControlFlowNode predecessor : node.getPredecessors()) {
            if (predecessor != null) {
                System.out.println(String.format("%s->%d",
                            thisAddr,
                            System.identityHashCode(predecessor)));
                outputGraphviz(predecessor);
            }
        }
    }

    @Override
    public final Set<String> getSupportedAnnotationTypes() {
        return Collections.singleton("*");
    }

    @Override
    public final SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_7;
    }
}
