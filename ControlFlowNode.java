import com.sun.source.tree.Tree;
import java.util.*;

/**
 * A single node in a control flow graph.
 * @author <a href="mailto:koenig@cs.washington.edu">David Matthew Jerry Koenig</a>
 */
public class ControlFlowNode {
    private ControlFlowNode elseSuccessor;
    private Map<Throwable, ControlFlowNode> exceptionSuccessors;
    private List<ControlFlowNode> predecessors;
    private ControlFlowNode successor;
    private Tree tree;

    /**
     * Sets up a control flow node with no predecessors or exception
     * successors.
     */
    public ControlFlowNode(Tree tree) {
        exceptionSuccessors = new HashMap<Throwable, ControlFlowNode>();
        predecessors = new ArrayList<ControlFlowNode>();
        this.tree = tree;
    }

    /**
     * If the current node is a condition in a loop, if statement, or the
     * ternary operator, the path to take if the condition is false.
     */
    public ControlFlowNode getElseSuccessor() {
        return elseSuccessor;
    }

    /**
     * A map from Throwables that may be thrown in the evaluation of this
     * node to the path that execution will take on the throwing of the
     * corresponding throwable.
     */
    public Map<Throwable, ControlFlowNode> getExceptionSuccessors() {
        return exceptionSuccessors;
    }

    /**
     * All incoming paths to this node.
     */
    public List<ControlFlowNode> getPredecessors() {
        return predecessors;
    }

    /**
     * The outgoing path of execution. If the current node is a condition in
     * a loop, if statement, or the ternary operator, the path to take if the
     * condition is true.
     */
    public ControlFlowNode getSuccessor() {
        return successor;
    }

    /**
     * Returns the node in the abstract syntax tree that corresponds to this
     * node in the control flow graph.
     */
    public Tree getTree() {
        return tree;
    }

    /**
     * Sets the following node for normal execution. If the current node is a
     * condition in a loop, if statement, or the ternary operator, the path to
     * take if the condition is true.
     */
    public void setSuccessor(ControlFlowNode successor) {
        this.successor = successor;
    }

    /**
     * Sets the path to take if the current node is a condition in a loop, if
     * statement, or the ternary operator, and the condition is false.
     */
    public void setElseSuccessor(ControlFlowNode elseSuccessor) {
        this.elseSuccessor = elseSuccessor;
    }

    /**
     * Sets the node in the abstract syntax tree that corresponds to this node
     * in the control flow graph.
     */
    public void setTree(Tree tree) {
        this.tree = tree;
    }
}
