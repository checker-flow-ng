import com.sun.source.tree.*;
import com.sun.source.util.*;
import java.util.Stack;

// TODO Handle exceptions everywhere

public class GenerateControlFlow extends TreePathScanner<ControlFlowNode, ControlFlowNode> {
    private Stack<ControlFlowNode> returnEndpoints;

    public GenerateControlFlow() {
        super();
        returnEndpoints = new Stack<ControlFlowNode>();
    }

    public ControlFlowNode scan(Tree tree, ControlFlowNode previous) {
//        System.out.println("TOREMOVE in scan");
        ControlFlowNode rv = super.scan(tree, previous);

        if (previous != null) {
            rv.getPredecessors().add(previous);
            previous.setSuccessor(rv);
        }

        return rv;
    }

    public ControlFlowNode visitAnnotation(AnnotationTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitArrayAccess(ArrayAccessTree tree, ControlFlowNode previous) {
        return scan(tree.getIndex(), scan(tree.getExpression(), previous));
    }

    public ControlFlowNode visitArrayType(ArrayTypeTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitAssert(AssertTree tree, ControlFlowNode previous) {
        return null; // TODO replace
        //return scan(tree.getCondition(), previous);
        // TODO: Handle case for detail
    }

    public ControlFlowNode visitAssignment(AssignmentTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitBinary(BinaryTree tree, ControlFlowNode previous) {
        System.out.println("TOREMOVE in binary");
        return scan(tree.getRightOperand(), scan(tree.getLeftOperand(), previous));
    }

    public ControlFlowNode visitBlock(BlockTree tree, ControlFlowNode previous) {
        System.out.println("TOREMOVE in block");
        // TODO: Handle case for static blocks
        ControlFlowNode current = previous;
        for (StatementTree stmt : tree.getStatements())
            current = scan(stmt, current);
        return current;
    }

    public ControlFlowNode visitBreak(BreakTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitCase(CaseTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitCatch(CatchTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitClass(ClassTree tree, ControlFlowNode previous) {
        System.out.println("TOREMOVE in class");
        for (Tree member : tree.getMembers()) {
            if (member != null && member.getKind() == Tree.Kind.METHOD);
                scan(member, null);
                // TODO s/null/previous/?
        }
        return previous;
    }

    public ControlFlowNode visitCompilationUnit(CompilationUnitTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitCompoundAssignment(CompoundAssignmentTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitConditionalExpression(ConditionalExpressionTree tree, ControlFlowNode previous) {
        ControlFlowNode condition = scan(tree.getCondition(), previous);

        ControlFlowNode ifTrue = scan(tree.getTrueExpression(), condition);
        condition.setSuccessor(ifTrue);

        // We can not use scan; it clobbers the successor, which is bad.
        // TODO encapsulate this?
        ControlFlowNode ifFalse = new ControlFlowNode(tree.getFalseExpression());
        ifFalse.getPredecessors().add(condition);
        condition.setElseSuccessor(ifFalse);

        ControlFlowNode join = new ControlFlowNode(null);
        join.getPredecessors().add(ifTrue);
        join.getPredecessors().add(ifFalse);
        ifTrue.setSuccessor(join);
        ifFalse.setSuccessor(join);

        return join;
    }

    public ControlFlowNode visitContinue(ContinueTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitDoWhileLoop(DoWhileLoopTree tree, ControlFlowNode previous) {
        // TODO reimplement
        return null; // TODO replace
    }

    public ControlFlowNode visitEmptyStatement(EmptyStatementTree tree, ControlFlowNode previous) {
        return previous;
    }

    public ControlFlowNode visitEnhancedForLoop(EnhancedForLoopTree tree, ControlFlowNode previous) {
        // TODO this is likely wrong too
        ControlFlowNode expression = scan(tree.getExpression(), previous);
        ControlFlowNode statement = scan(tree.getStatement(), expression);

        // We can not use scan; it clobbers the successor, which is bad.
        // TODO encapsulate this?
        ControlFlowNode split = new ControlFlowNode(null);
        split.getPredecessors().add(statement);
        expression.getPredecessors().add(split);
        statement.setSuccessor(split);
        split.setElseSuccessor(expression);

        return split;
    }

    public ControlFlowNode visitErroneous(ErroneousTree tree, ControlFlowNode previous) {
        // TODO Yeah, right.
        return null;
    }

    public ControlFlowNode visitExpressionStatement(ExpressionStatementTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitForLoop(ForLoopTree tree, ControlFlowNode previous) {
        ControlFlowNode init = previous;
        for (StatementTree stmt : tree.getInitializer())
            init = scan(stmt, init);
        ControlFlowNode condition = scan(tree.getCondition(), init);
        ControlFlowNode statement = scan(tree.getStatement(), condition);
        ControlFlowNode update = statement;
        for (ExpressionStatementTree stmt : tree.getUpdate())
            update = scan(stmt, update);

        condition.getPredecessors().add(update);
        update.setSuccessor(condition);

        ControlFlowNode dummy = new ControlFlowNode(null);
        dummy.getPredecessors().add(condition);
        condition.setElseSuccessor(dummy);

        return dummy;
    }

    public ControlFlowNode visitIdentifier(IdentifierTree tree, ControlFlowNode previous) {
        ControlFlowNode rv = new ControlFlowNode(tree);

        if (previous != null) {
            rv.getPredecessors().add(previous);
            previous.setSuccessor(previous);
        }

        return rv;
    }

    public ControlFlowNode visitIf(IfTree tree, ControlFlowNode previous) {
        ControlFlowNode condition = scan(tree.getCondition(), previous);

        ControlFlowNode thenBlock = scan(tree.getThenStatement(), condition);
        condition.setSuccessor(thenBlock);

        // We can not use scan; it clobbers the successor, which is bad.
        // TODO encapsulate this?
        ControlFlowNode elseBlock = new ControlFlowNode(tree.getElseStatement());
        elseBlock.getPredecessors().add(condition);
        condition.setElseSuccessor(elseBlock);

        ControlFlowNode join = new ControlFlowNode(null);
        join.getPredecessors().add(thenBlock);
        join.getPredecessors().add(elseBlock);
        thenBlock.setSuccessor(join);
        elseBlock.setSuccessor(join);

        return join;
    }

    public ControlFlowNode visitImport(ImportTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitInstanceOf(InstanceOfTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitLabeledStatement(LabeledStatementTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitLiteral(LiteralTree tree, ControlFlowNode previous) {
        System.out.println("TOREMOVE in literal");
        ControlFlowNode rv = new ControlFlowNode(tree);

        if (previous != null) {
            rv.getPredecessors().add(previous);
            previous.setSuccessor(previous);
        }

        return rv;
    }

    public ControlFlowNode visitMemberSelect(MemberSelectTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitMethod(MethodTree tree, ControlFlowNode previous) {
        // TODO it's probably a bit more complex than this

        ControlFlowNode endpoint = new ControlFlowNode(null);
        returnEndpoints.push(endpoint);
        ControlFlowNode rv = scan(tree.getBody(), previous);
        returnEndpoints.pop();

        return rv;
    }

    public ControlFlowNode visitMethodInvocation(MethodInvocationTree tree, ControlFlowNode previous) {
        System.out.println("TOREMOVE in method invocation");
        ControlFlowNode current = previous;
        for (ExpressionTree expr : tree.getArguments())
            current = scan(expr, current);
        return current;
    }

    public ControlFlowNode visitModifiers(ModifiersTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitNewArray(NewArrayTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitNewClass(NewClassTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitOther(Tree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitParameterizedType(ParameterizedTypeTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitParenthesized(ParenthesizedTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitPrimitiveType(PrimitiveTypeTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitReturn(ReturnTree tree, ControlFlowNode previous) {
        ControlFlowNode rv = scan(tree.getExpression(), previous);
        ControlFlowNode endOfBlock = returnEndpoints.peek();
        rv.setSuccessor(endOfBlock);
        endOfBlock.getPredecessors().add(rv);
        return rv;
    }

    public ControlFlowNode visitSwitch(SwitchTree tree, ControlFlowNode previous) {
        return null; // TODO replace
        //ControlFlowNode current = scan(tree.getExpression(), previous);
        // TODO First thought was to cheat and call it a bunch of if statements, but that
        // won't work since not all cases have breaks at the end. Looks like we didn't get
        // out of goto after all :(
    }

    public ControlFlowNode visitSynchronized(SynchronizedTree tree, ControlFlowNode previous) {
        return scan(tree.getBlock(), scan(tree.getExpression(), previous));
    }

    public ControlFlowNode visitThrow(ThrowTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitTry(TryTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitTypeCast(TypeCastTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitTypeParameter(TypeParameterTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitUnary(UnaryTree tree, ControlFlowNode previous) {
        return scan(tree.getExpression(), previous);
    }

    public ControlFlowNode visitVariable(VariableTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }

    public ControlFlowNode visitWhileLoop(WhileLoopTree tree, ControlFlowNode previous) {
        ControlFlowNode condition = scan(tree.getCondition(), previous);
        ControlFlowNode statement = scan(tree.getStatement(), condition);
        condition.getPredecessors().add(statement);
        statement.setSuccessor(condition);

        ControlFlowNode dummy = new ControlFlowNode(null);
        dummy.getPredecessors().add(condition);
        condition.setElseSuccessor(dummy);

        return dummy;
    }

    public ControlFlowNode visitWildcard(WildcardTree tree, ControlFlowNode previous) {
        return null; // TODO replace
    }
}
